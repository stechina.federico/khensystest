namespace KhensysTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Permisos",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NombreEmpleado = c.String(nullable: false, maxLength: 50),
                        ApellidosEmpleado = c.String(nullable: false, maxLength: 50),
                        TipoPermiso = c.Int(nullable: false),
                        FechaPermiso = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TipoPermisoes", t => t.TipoPermiso)
                .Index(t => t.TipoPermiso);
            
            CreateTable(
                "dbo.TipoPermisoes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Descripcion = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.sysdiagrams",
                c => new
                    {
                        diagram_id = c.Int(nullable: false, identity: true),
                        name = c.String(nullable: false, maxLength: 128),
                        principal_id = c.Int(nullable: false),
                        version = c.Int(),
                        definition = c.Binary(),
                    })
                .PrimaryKey(t => t.diagram_id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Permisos", "TipoPermiso", "dbo.TipoPermisoes");
            DropIndex("dbo.Permisos", new[] { "TipoPermiso" });
            DropTable("dbo.sysdiagrams");
            DropTable("dbo.TipoPermisoes");
            DropTable("dbo.Permisos");
        }
    }
}
