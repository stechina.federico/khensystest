﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using KhensysTest.Models;
using KhensysTest.Services;

namespace KhensysTest.Controllers
{
    public class PermisosController : Controller
    {
        
        private PermisosContext db = new PermisosContext();
        private readonly IPermisos _permisos;

        public PermisosController(IPermisos permisos)
        {
            _permisos = permisos;
        }


        // GET: Permisos
        [HttpGet]
        public ActionResult Index()
        {
            return View( _permisos.GetPermisos());
        }


       // GET: Permisos/Create
        public ActionResult Create()
        {
            ViewBag.TipoPermiso = new SelectList(db.TipoPermiso, "Id", "Descripcion");
            return View();
        }


        // POST: Permisos/Create      
        [HttpPost]
        public ActionResult Create(Permisos permisos)
        {
            if (ModelState.IsValid)
            {
                _permisos.CreatePermisos(permisos);
                             
                return RedirectToAction("Index");
            }

            ViewBag.TipoPermiso = new SelectList(db.TipoPermiso, "Id", "Descripcion", permisos.TipoPermiso);
            return View(permisos);
        }

        [HttpPost]
        public async Task<ActionResult> Delete(int id)
        {
           await _permisos.DeletePermisos(id);
            return View();
        }







        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
