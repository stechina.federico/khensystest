﻿
using KhensysTest.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace KhensysTest.Services
{
    public interface IPermisos
    {       
        IEnumerable<Permisos> GetPermisos();
        Task<Permisos> CreatePermisos(Permisos permisos);
        Task<Permisos> DeletePermisos(int id);
    }


    public class PermisosService : IPermisos
    {

        private PermisosContext _dbContext;

        public PermisosService(PermisosContext dbContext)
        {
            _dbContext = dbContext;
        }

       
        public IEnumerable<Permisos> GetPermisos()
        {
            return _dbContext.Permisos.Include(p => p.TipoPermisos).ToList();
         
        }


        public async Task<Permisos> CreatePermisos(Permisos permisos)
        {
            _dbContext.Permisos.Add(permisos);
            await _dbContext.SaveChangesAsync();

            return permisos;
        }


        public async Task<Permisos> DeletePermisos(int id)
        {
            var permisos = await _dbContext.Permisos.FindAsync(id);
            _dbContext.Permisos.Remove(permisos);
            await _dbContext.SaveChangesAsync();

            return permisos;
        }
    }
}
