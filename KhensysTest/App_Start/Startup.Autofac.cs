﻿using Autofac;
using Autofac.Integration.Mvc;
using KhensysTest.Models;
using KhensysTest.Services;
using Owin;
using System.Data.Entity;
using System.Web.Mvc;

namespace KhensysTest
{
    public partial class Startup
    {
        public IContainer Container { get; set; }

        public void InitializeContainer(IAppBuilder app)
        {
            // Create your builder.
            var builder = new ContainerBuilder();

            builder.RegisterType<PermisosService>().As<IPermisos>();
            builder.RegisterControllers(typeof(MvcApplication).Assembly);
            builder.RegisterType<PermisosContext>().AsSelf().As<DbContext>();

            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));

            //builder.RegisterSource(new AnyConcreteTypeNotAlreadyRegisteredSource());
            //var assembly = Assembly.GetExecutingAssembly();
            //builder.RegisterAssemblyTypes(assembly)
            //      .Where(t => t.Name.EndsWith("Service"))
            //      .AsImplementedInterfaces();
        }
    }
}