namespace KhensysTest.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class PermisosContext : DbContext
    {
        public PermisosContext()
            : base("name=PermisosContext")
        {
        }

        public virtual DbSet<Permisos> Permisos { get; set; }      
        public virtual DbSet<TipoPermiso> TipoPermiso { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TipoPermiso>()
                .HasMany(e => e.Permisos)
                .WithRequired(e => e.TipoPermisos)
                .HasForeignKey(e => e.TipoPermiso)
                .WillCascadeOnDelete(false);
        }
    }
}
