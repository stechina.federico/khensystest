namespace KhensysTest.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Permisos
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Debe ingresar el Nombre")]
        [StringLength(50)]
        public string NombreEmpleado { get; set; }

        [Required(ErrorMessage = "Debe ingresar el Apellido")]
        [StringLength(50)]
        public string ApellidosEmpleado { get; set; }

        public int TipoPermiso { get; set; }

        [Required(ErrorMessage = "Debe ingresar una Fecha")]
        public DateTime FechaPermiso { get; set; }

        public virtual TipoPermiso TipoPermisos { get; set; }
    }
}
