namespace KhensysTest.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

   
    public partial class TipoPermiso
    {       
        public TipoPermiso()
        {
            Permisos = new HashSet<Permisos>();
        }

      
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Descripcion { get; set; }
       
        public virtual ICollection<Permisos> Permisos { get; set; }
    }
}
